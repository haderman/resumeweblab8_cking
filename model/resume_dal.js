var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT account.first_name, account.last_name, account.account_id, resume.resume_name, resume.resume_id FROM resume ' +
                'LEFT JOIN account on account.account_id = resume.account_id ' +
                'ORDER BY account.first_name ';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.add = function(account_id, callback) {
    var query = 'CALL add_resume(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.allUsers = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(resume_id, callback) {
    var query = 'CALL resume_getbyid(?)';


    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};



exports.edit = function(account_id, callback) {
    var query = 'CALL edit_resume(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};



exports.insertzzz = function(params, callback) {

    // FIRST INSERT THE address
    var query = 'INSERT INTO resume(email, first_name, last_name) VALUES ?';
    var accountData = [];


    accountData.push([params.email, params.first_name, params.last_name]);


    connection.query(query, [accountData], function(err, result){
        callback(err, result);
    });
};


exports.insert = function(params, callback) {


    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO resume (resume_name) VALUES (?)';


    var queryData = [params.resume_name, params.account_id, params.school_id, params.skill_id, params.company_id, params.resume_id];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var resume_id = result.insertId;


        var query = 'UPDATE resume SET account_id = ? WHERE resume_id = ?';


                if (params.school_id != null) {
                    //insert company_address ids
                    resumeSchoolInsert(resume_id, params.school_id, function (err, result) {
                        connection.query(query, queryData, function (err, result) {
                            //delete account_skill entries for this account


                            if (params.skill_id != null) {
                                //insert account_skill ids
                                resumeSkillInsert(resume_id, params.skill_id, function (err, result) {
                                    connection.query(query, queryData, function (err, result) {


                                        if (params.company_id != null) {
                                            //insert account_skill ids
                                            resumeCompanyInsert(resume_id, params.company_id, function (err, result) {
                                                callback(err, resume_id);
                                            });
                                        }


                                    });
                                });
                            }


                        });
                    });
                }



    });


};




var resumeAccountUpdate = function(resume_id, accountIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'UPDATE resume SET account_id = ? WHERE resume.resume_id = resume_id';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeAccountData = [];
    if (accountIdArray.constructor === Array) {
        for (var i = 0; i < schoolIdArray.length; i++) {
            resumeAccountData.push([resume_id, accountIdArray[i]]);
        }
    }
    else {
        resumeAccountData.push([resume_id, accountIdArray]);
    }
    connection.query(query, [resumeAccountData], function(err, result){
        callback(err, result);
    });
};


var resumeSchoolInsert = function(resume_id, schoolIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeSchoolData = [];
    if (schoolIdArray.constructor === Array) {
        for (var i = 0; i < schoolIdArray.length; i++) {
            resumeSchoolData.push([resume_id, schoolIdArray[i]]);
        }
    }
    else {
        resumeSchoolData.push([resume_id, schoolIdArray]);
    }
    connection.query(query, [resumeSchoolData], function(err, result){
        callback(err, result);
    });
};
var resumeSkillInsert = function(resume_id, skillIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeSkillData = [];
    if (skillIdArray.constructor === Array) {
        for (var i = 0; i < skillIdArray.length; i++) {
            resumeSkillData.push([resume_id, skillIdArray[i]]);
        }
    }
    else {
        resumeSkillData.push([resume_id, skillIdArray]);
    }
    connection.query(query, [resumeSkillData], function(err, result){
        callback(err, result);
    });
};
var resumeCompanyInsert = function(resume_id, companyIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeCompanyData = [];
    if (companyIdArray.constructor === Array) {
        for (var i = 0; i < companyIdArray.length; i++) {
           resumeCompanyData.push([resume_id, companyIdArray[i]]);
        }
    }
    else {
        resumeCompanyData.push([resume_id, companyIdArray]);
    }
    connection.query(query, [resumeCompanyData], function(err, result){
        callback(err, result);
    });
};

var resumeSchoolDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_school WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
var resumeSkillDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_skill WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
var resumeCompanyDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_company WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resume_name = ? WHERE resume_id = ?';
    var queryData = [params.resume_name];



    connection.query(query, queryData, function(err, result) {
        //delete account_school entries for this account
        resumeSchoolDeleteAll(params.resume_id, function(err, result){

            if(params.school_id != null) {
                //insert company_address ids
                resumeSchoolInsert(params.resume_id, params.school_id, function(err, result){
                    connection.query(query, queryData, function(err, result) {
                        //delete resume_skill entries for this resume
                        resumeSkillDeleteAll(params.resume_id, function(err, result){

                            if(params.skill_id != null) {
                                //insert resume_skill ids
                                resumeSkillInsert(params.resume_id, params.skill_id, function(err, result){
                                    connection.query(query, queryData, function(err, result) {
                                        resumeCompanyDeleteAll(params.resume_id, function(err, result){

                                            if(params.company_id != null) {
                                                //insert resume_skill ids
                                                resumeCompanyInsert(params.resume_id, params.company_id, function(err, result){
                                                    callback(err, result);
                                                });}

                                        });

                                    });
                                });}

                        });
                    });
                });}

        });
    });


};


exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};